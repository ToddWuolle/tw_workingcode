import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;


public class FileRenamer {

	 protected static char[] goodChar = {  
	        // Comment out next two lines to make upper-case-only, then  
	        // use String toUpper() on the user's input before validating.  
	        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n',  
	        'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',  
	       // 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N',  
	       // 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',  
	      //  '2', '3', '4', '5', '6', '7', '8', '9',  
	      //  '+', '-', '@',  
	    };  
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("hello directory : " + args[0]);
		
		File directory = new File(args[0]);   
		String fileType = args[1].trim().toLowerCase();
		
		List<String> fileNames = new ArrayList<String>();
		
		listFilesForFolder(directory,fileType , fileNames);
		
		
		
	}
	
	
	public static  void listFilesForFolder(final File folder, final String fileType, final List<String> fileNames) {
		String temp;
		
		Set<String> usedFileNames = new TreeSet<String>();

		
		
		
		
		
		
		
	    for (final File fileEntry : folder.listFiles()) {
	      if (fileEntry.isDirectory()) {
	        // System.out.println("Reading files under the folder "+folder.getAbsolutePath());
	        listFilesForFolder(fileEntry, fileType, fileNames);
	      } else {
	        if (fileEntry.isFile()) {
	          temp = fileEntry.getName();
	          
	          if (fileType != null 
	        		  && (temp.substring(temp.lastIndexOf('.') + 1, temp.length()).toLowerCase()).equals(fileType)){
	            
	        	  	//System.out.println("File= " + folder.getAbsolutePath()+ "\\" + fileEntry.getName());
	        	  	final Random random = new Random(System.nanoTime());
	        	    int randomNumber = random.nextInt(goodChar.length - 1);
	        	    
	        	    while (randomNumber > 0 && randomNumber > goodChar.length)
	        	   
	        	    {
	        	    	randomNumber = random.nextInt(goodChar.length - 1);
	        	    }
	        	    
	        	    
	        	    String fileName = getRandomFileName(randomNumber,usedFileNames);
	        	  	usedFileNames.add(fileName);
	        	  	
	        	  	//System.out.println("random file name: " + randomUUID.toString());
	        	  	File newFile = new File(fileEntry.getParent() + "\\" + fileName + ".jpg");
	        	  	
	        	  	fileEntry.renameTo(newFile);
	        	  	fileNames.add(fileEntry.getName());
	          }
	           
	        }

	      }
	    }
	    
	    if (!usedFileNames.isEmpty())
	    {
	    	System.out.println("file names to used : ");
	    	
	    	for (String file : usedFileNames)
	    	{
	    		System.out.println(file);
	    	}
	    }
	  }
	
	private static String getRandomFileName(int start, Set<String>knownNames)
	{
		StringBuilder fileName = new StringBuilder("");
		
		boolean goodFile = false;
		while (!goodFile)
		{
			for (int i = start; i < goodChar.length ; i++)
			{
				
				fileName.append(goodChar[i]);
			}
			
			if (!knownNames.contains(fileName.toString()))
			{
				goodFile = true;
			}
			{
				final Random random = new Random(System.nanoTime());
        	    int randomNumber = random.nextInt(goodChar.length - 1);
        	    
        	    while (randomNumber > 0 && randomNumber > goodChar.length)
        	   
        	    {
        	    	randomNumber = random.nextInt(goodChar.length - 1);
        	    	
        	    }
        	    start = randomNumber;
			}
			
		}
		
		return fileName.toString();
		
		
		
		
	}
}
