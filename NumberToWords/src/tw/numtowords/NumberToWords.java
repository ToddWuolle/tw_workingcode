package tw.numtowords;

import java.text.DecimalFormat;

/**
 * <p>
 * Number to words parsing library. Converts integer values into equivalent
 * words. For example, 545 input would be five hundred forty-five. Usage :
 * 
 * String wordsFromNumbers = NumberToWords.convertInteger(545);
 * </p>
 * 
 * 
 * or
 * <p>
 * String wordsFromNumbers =
 * NumberToWords.convertInteger(Integer.valueOf(-545));
 * 
 * negative five hundred forty-five
 * </p>
 * 
 * @author Todd Wuolle
 *
 */
public class NumberToWords {

	private static final int HUNDRED_MODULUS_VALUE = 100;

	private static final int HUNDRED_DIVISOR = 100;

	private static final int SPECIAL_NUMBER_NAME_CUTOFF = 20;

	private static final int BILLIONS_MAX_INDEX = 3;

	private static final int MILLIONS_MAX_INDEX = 6;

	private static final int HUNDRED_THOUSANDS_MAX_INDEX = 9;

	private static final int ONES_MAX_INDEX = 12;

	private static final String[] tensInWords = { "", " ten", " twenty", " thirty", " forty", " fifty", " sixty",
			" seventy", " eighty", " ninety" };

	private static final String[] numberWords = { "", "one", "two", "three", "four", "five", "six", "seven", "eight",
			"nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
			"nineteen" };

	/**
	 * processLessThanThousand method Convert an input number that is less than
	 * 1000 into words
	 * 
	 * @param int
	 *            number
	 * @return String - equivalent words of input number
	 */
	protected static String processLessThanThousand(int number) {
		String conversionSoFar = "";

		// if number % 100 < 20, then find number word (ie. nineteen)
		if (number % HUNDRED_MODULUS_VALUE < SPECIAL_NUMBER_NAME_CUTOFF) {

			conversionSoFar = numberWords[number % HUNDRED_MODULUS_VALUE];
			number /= HUNDRED_DIVISOR;
		} else {
			// add a dash between so end up with twenty-one
			if (number % 10 != 0) {
				conversionSoFar = conversionSoFar + "-";
			}

			conversionSoFar = conversionSoFar + numberWords[number % 10];

			number /= 10;

			conversionSoFar = tensInWords[number % 10] + conversionSoFar;

			number /= 10;

		}
		if (number == 0) {
			return conversionSoFar;
		}
		return numberWords[number] + " hundred" + conversionSoFar;
	}

	/**
	 * Wrapper method, to accept primitive int instead of Integer
	 * 
	 * @param int
	 *            inputValue
	 * @return result of conversion
	 */
	public static final String convertIntegerToWords(final int inputValue) {
		return convertIntegerToWords(Integer.valueOf(inputValue));
	}

	/**
	 * Convert integer to words Take input integer, convert to equivalent words.
	 * Should work for all valid Integer values, including negative
	 * 
	 * @param inputNumber
	 * @return String with word equivalents
	 */
	public static final String convertIntegerToWords(final Integer inputNumber) {
		String prefix = "";
		String conversionResult = "";

		int numberValue = 0;

		int negativeOffset = 0;

		if (inputNumber != null) {
			if (inputNumber.intValue() == 0) {
				return "zero";
			} else if (inputNumber.intValue() < 0) {
				prefix = "negative ";

				negativeOffset = 1;

			}

			numberValue = inputNumber.intValue();

		} else {
			return "empty";
		}

		// pad with "0" to make known length String. Max value of Integer is 2B
		// etc.
		String mask = "000000000000";

		DecimalFormat df = new DecimalFormat(mask);
		String stringNumber = df.format(Integer.valueOf(numberValue));

		// grab first 3 numbers, unless a negative number than we need to use
		// offset to account for - sign. If negative, negativeOffset would be 1

		int billions = grabNumberChunk(negativeOffset, BILLIONS_MAX_INDEX + negativeOffset, stringNumber);

		// grab next 3

		int millions = grabNumberChunk(BILLIONS_MAX_INDEX + negativeOffset, MILLIONS_MAX_INDEX + negativeOffset,
				stringNumber);

		// grab next 3

		int hundredThousands = grabNumberChunk(MILLIONS_MAX_INDEX + negativeOffset,
				HUNDRED_THOUSANDS_MAX_INDEX + negativeOffset, stringNumber);

		// grab last numbers

		int thousands = grabNumberChunk(HUNDRED_THOUSANDS_MAX_INDEX + negativeOffset, ONES_MAX_INDEX + negativeOffset,
				stringNumber);

		String result = processNumberChunk(billions, " billion ");

		result = result + processNumberChunk(millions, " million ");

		// special case
		String translatedHunThousands;
		switch (hundredThousands) {
		case 0:
			translatedHunThousands = "";
			break;
		case 1:
			translatedHunThousands = "one thousand ";
			break;
		default:
			translatedHunThousands = processLessThanThousand(hundredThousands) + " thousand ";
		}
		result = result + translatedHunThousands;

		// process last numbers < 1000
		String parseThousand;
		parseThousand = processLessThanThousand(thousands);
		result = result + parseThousand;

		// clean up result string
		conversionResult = prefix + result.replaceAll("^\\s+", "");

		return conversionResult;
	}

	/**
	 * Utility method to grab indicated parts of input number string
	 * 
	 * @param startIndex
	 * @param endIndex
	 * @param numberString
	 * @return int representing part of number in between start and end index
	 */
	public static int grabNumberChunk(final int startIndex, final int endIndex, final String numberString) {

		if (numberString == null) {
			return 0;
		}

		return Integer.parseInt(numberString.substring(startIndex, endIndex));
	}

	/**
	 * Process section of number, setting correct word equivalent
	 * 
	 * @param int
	 *            valueToTest
	 * @param String
	 *            defaultValue
	 * @return String , words equivalent to number
	 */
	public static String processNumberChunk(final int valueToTest, final String defaultValue) {
		String chunkResult;

		switch (valueToTest) {
		case 0:
			chunkResult = "";
			break;
		case 1:
			chunkResult = processLessThanThousand(valueToTest) + defaultValue;
			break;
		default:
			chunkResult = processLessThanThousand(valueToTest) + defaultValue;
		}

		return chunkResult;
	}
}