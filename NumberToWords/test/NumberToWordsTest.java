import static org.junit.Assert.assertTrue;

import org.junit.Test;

import tw.numtowords.NumberToWords;

public class NumberToWordsTest {

	@Test
	public void testConvert() {

		String EXPECTED_MIN_VALUE = "negative two billion one hundred forty-seven million four hundred eighty-three thousand six hundred forty-eight";

		String EXPECTED_MAX_VALUE = "two billion one hundred forty-seven million four hundred eighty-three thousand six hundred forty-seven";

		String EXPECTED_ZERO_VALUE = "zero";

		assertTrue(NumberToWords.convertIntegerToWords(Integer.valueOf(Integer.MIN_VALUE)).equals(EXPECTED_MIN_VALUE));
		assertTrue(NumberToWords.convertIntegerToWords(Integer.valueOf(Integer.MAX_VALUE)).equals(EXPECTED_MAX_VALUE));

		assertTrue(NumberToWords.convertIntegerToWords(0).equals(EXPECTED_ZERO_VALUE));

		System.out.println(" test min : " + NumberToWords.convertIntegerToWords(Integer.valueOf(Integer.MIN_VALUE)));

		System.out.println(" test max : " + NumberToWords.convertIntegerToWords(Integer.valueOf(Integer.MAX_VALUE)));

		System.out.println("empty value : " + NumberToWords.convertIntegerToWords(null));

	}

	@Test
	public void testGrabNumberChunk() {
		int expectedChunk1 = 34344;

		int expectedChunk2 = 22221;

		String numberString = "3434422221";
		
		
		assertTrue(NumberToWords.grabNumberChunk(0, 5, numberString) == expectedChunk1);

		assertTrue(NumberToWords.grabNumberChunk(5, 10, numberString) == expectedChunk2);
		System.out.println("chunk 1 : " + NumberToWords.grabNumberChunk(0, 5, numberString));
	}

	@Test
	public void testProcessNumberChunk() {
		int testInput = 10;

		String expectedOutput = "ten thousand ";

		System.out.println(" Thousands output: " + NumberToWords.processNumberChunk(testInput, " thousand "));

		assertTrue(NumberToWords.processNumberChunk(testInput, " thousand ").equals(expectedOutput));

	}

}
